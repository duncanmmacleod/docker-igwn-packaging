FROM containers.ligo.org/docker/base:el7

LABEL name="LIGO - Koji Testing - EL7 Production" \
      maintainer="Adam Mercer <adam.mercer@ligo.org>" \
      support="Reference Platform"

# remove upstream lscsoft-{production-backports} repositories
RUN yum -y remove lscsoft-production-config \
        lscsoft-production-debug-config \
        lscsoft-backports-config \
        lscsoft-backports-debug-config && \
    yum clean all && rm -rf var/cache/yum/x86_64/7/*

# add lscsoft-backports repository
RUN echo "[lscsoft-backports]" > /etc/yum.repos.d/backports.repo && \
    echo "name = lscsoft-backports" >> /etc/yum.repos.d/backports.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/mash/lscsoft/\$releasever/\$basearch/backports/" >> /etc/yum.repos.d/backports.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/backports.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/backports.repo

# add lscsoft-backports-debug repository
RUN echo "[lscsoft-backports-debug]" > /etc/yum.repos.d/backports-debug.repo && \
    echo "name = lscsoft-backports-debug" >> /etc/yum.repos.d/backports-debug.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/mash/lscsoft/\$releasever/debug/backports/" >> /etc/yum.repos.d/backports-debug.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/backports-debug.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/backports-debug.repo

# add lscsoft-production repository
RUN echo "[lscsoft-production]" > /etc/yum.repos.d/production.repo && \
    echo "name = lscsoft-production" >> /etc/yum.repos.d/production.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/mash/lscsoft/\$releasever/\$basearch/production/" >> /etc/yum.repos.d/production.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/production.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/production.repo

# add lscsoft-production-debug repository
RUN echo "[lscsoft-production-debug]" > /etc/yum.repos.d/production-debug.repo && \
    echo "name = lscsoft-production-debug" >> /etc/yum.repos.d/production-debug.repo && \
    echo "baseurl = https://koji.ligo-la.caltech.edu/mash/lscsoft/\$releasever/debug/production/" >> /etc/yum.repos.d/production-debug.repo && \
    echo "enabled = 1" >> /etc/yum.repos.d/production-debug.repo && \
    echo "gpgcheck = 0" >> /etc/yum.repos.d/production-debug.repo

# install lscsoft-all, ldg-client, and a working shell
RUN yum -y install \
        bash-completion \
        ldg-client \
        lscsoft-all \
        nds2-server \
        yum-priorities && \
    yum clean all
